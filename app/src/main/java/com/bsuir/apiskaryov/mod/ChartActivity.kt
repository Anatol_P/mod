package com.bsuir.apiskaryov.mod

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.BarGraphSeries
import com.jjoe64.graphview.series.DataPoint

class ChartActivity : AppCompatActivity() {

        override fun onCreate(savedInstanceState: Bundle?) {
            val count = intent.getIntExtra("count", 0)
            val dispersion = intent.getDoubleExtra("dispersion", 0.0)
            val expectation = intent.getDoubleExtra("expectation", 0.0)
            val deviation = intent.getDoubleExtra("deviation", 0.0)
            val step = intent.getFloatExtra("step", 0.049f)
            val data = intent.getFloatArrayExtra("data").toList()
                    .mapIndexed { index, i ->
                        DataPoint(step.toDouble() * (index + 1), i.toDouble())
                    }
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_chart)
            val graph = findViewById<GraphView>(R.id.graph)
            val series = BarGraphSeries<DataPoint>(data.toTypedArray())
            graph.addSeries(series)
            findViewById<TextView>(R.id.count).append(count.toString())
            findViewById<TextView>(R.id.dispersion).append(dispersion.toString())
            findViewById<TextView>(R.id.expectation).append(expectation.toString())
            findViewById<TextView>(R.id.deviation).append(deviation.toString())
        }
}
