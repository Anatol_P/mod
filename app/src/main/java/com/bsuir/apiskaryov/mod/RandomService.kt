package com.bsuir.apiskaryov.mod


/**
 * Created by a.piskaryov on 11/21/2017.
 */
object RandomNumbers {
    fun lemer(a: Int, m: Int, R0: Int): Coefficients {
        var n = 0
        var randNum: Int
        val coefficientMass = mutableListOf(R0)
        val randomMass = mutableListOf<Float>()
        while (true) {
            randNum = (a * coefficientMass[n])
            coefficientMass.add(randNum % m)
            (coefficientMass[n + 1].toFloat() / m.toFloat()).let {
                randomMass.add(it)
            }
            if ((coefficientMass[0] == coefficientMass[n + 1]) || (coefficientMass.size >= 2000000)) {
                return Coefficients(coefficientMass, randomMass)
            }
            n++
        }

    }

     fun calculateDataForGist(coefficients: Coefficients): Gist {
        val dataForGist = FloatArray(20)
        val step = (coefficients.randomMass.max()!!.minus(coefficients.randomMass.min()!!)) / 20
        var X: Int
        coefficients.randomMass.forEach {
            X = (it / step).toInt()
            if (X >= dataForGist.size) X = dataForGist.size - 1
            dataForGist[X] += 1f
        }
        dataForGist.map { it ->
             it / coefficients.randomMass.size
        }.let {
            return Gist(step, it.toFloatArray())
        }

    }

    fun properties(randomMass: List<Float> ): Properties {
        val expectation = randomMass.sum().toDouble() / randomMass.size
        val dispersion = randomMass.map { Math.pow(it.toDouble() - expectation, 2.0) }.sum() / (randomMass.size -1)
        val deviation = Math.sqrt(dispersion)
        return Properties(expectation, dispersion, deviation, randomMass.size)
    }
}

data class Coefficients(val coefficientMass: List<Int>, val randomMass: List<Float>)
data class Gist(val step: Float, val data: FloatArray)
data class Properties(val expectation: Double, val dispersion: Double, val deviation: Double, val count: Int)