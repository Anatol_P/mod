package com.bsuir.apiskaryov.mod

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    private val okBtn: Button by lazy { findViewById<Button>(R.id.goBtn) }
    private val aInput: EditText by lazy { findViewById<EditText>(R.id.a) }
    private val mInput: EditText by lazy { findViewById<EditText>(R.id.m) }
    private val r0Input: EditText by lazy { findViewById<EditText>(R.id.R0) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        okBtn.setOnClickListener {
            val a = aInput.text.toString().toInt()
            val m = mInput.text.toString().toInt()
            val R0 = r0Input.text.toString().toInt()
            processResult(a, m, R0)

        }
    }


    fun processResult(a: Int, m: Int, R0: Int) {
        val coeff = RandomNumbers.lemer(a, m, R0)
        val stats = RandomNumbers.calculateDataForGist(coeff)
        val props = RandomNumbers.properties(coeff.randomMass)
        Intent(baseContext, ChartActivity::class.java).let {
            it.putExtra("data", stats.data)
            it.putExtra("step", stats.step)
            it.putExtra("deviation", props.deviation)
            it.putExtra("expectation", props.expectation)
            it.putExtra("dispersion", props.dispersion)
            it.putExtra("count", props.count)
            startActivity(it)
        }
    }
}
